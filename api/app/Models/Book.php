<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    use HasFactory;
    protected $guarded = [];

    protected $casts = [
        'is_borrowed' => 'boolean' # mysql returns boolean as 0 / 1
    ];

    public function author() {
        return $this->belongsTo(Author::class);
    }
}
