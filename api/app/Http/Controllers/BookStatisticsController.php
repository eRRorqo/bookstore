<?php

namespace App\Http\Controllers;

use App\Models\Book;

class BookStatisticsController extends Controller
{
    /**
     * Return default statistics for cards in books grid view
     * @return Response $data
     */
    public function defaultBooksStatistics() {
        $data['total'] = Book::count();
        $data['is_borrowed'] = Book::where('is_borrowed', true)->count();
        $data['stock'] = Book::where('is_borrowed', false)->count();
        return response()->json($data);
    }
}
