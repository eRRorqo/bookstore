import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import axios from 'axios'
import VueAxios from 'vue-axios'
import './assets/style.scss'
axios.defaults.baseURL = 'http://161.35.216.140/bookstoreapi/public/api';
createApp(App).use(router).use(VueAxios, axios).mount('#app')
