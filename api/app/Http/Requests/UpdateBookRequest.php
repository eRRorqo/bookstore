<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateBookRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'book.id' => 'required',
            'book.title' => 'required',
            'book.author_id' => 'required|exists:authors,id',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'book.id.required' => trans('book.id.required'),
            'book.title.required' => trans('book.title.required'),
            'book.author_id.required' => trans('book.author_id.required'),
            'book.author_id.exists' => trans('book.author_id.exists'),
        ];
    }
}
