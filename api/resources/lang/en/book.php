<?php

return [
    'id' => [
        'required' => 'ID je povinné'
    ],
    'title' => [
        'required' => 'Názov je povinný',
        'unique' => 'Kniha s týmto názvom už existuje'
    ],
    'author_id' => [
        'required' => 'Autor je povinný',
        'exists' => 'Vybraný autor neexistuje'
    ]
];