<?php

namespace App\Http\Controllers;

use App\Models\Author;

class AuthorStatisticsController extends Controller
{
    /**
     * Return default statistics for cards in authors grid view
     * @return Response $data
     */
    public function defaultAuthorsStatistics() {
        $data['total'] = Author::count();
        return response()->json($data);
    }
}
