<?php

return [
    'id' => [
        'required' => 'ID je povinné'
    ],
    'name' => [
        'required' => 'Meno je povinné'
    ],
    'surname' => [
        'required' => 'Priezvisko je povinné'
    ]
];