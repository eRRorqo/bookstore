import { ref } from 'vue'
import { useRouter, useRoute } from 'vue-router'
import axios from "axios"

const Authors = () => {

    const router = useRouter();
    const route = useRoute();

    let responseData = ref([]);
    let errors = ref([]);
    let statistics = ref({})

    let authors = ref([]);
    let author= ref({
        id: null,
        name: "",
        surname: ""
    })

    /**
     * Get all authors
     */
    const getAll = (urlLink = 'authors') => {
        axios.get(urlLink)
            .then(response => {
                authors.value = response.data.data
                responseData.value = response.data
            })
            .catch(err => {
                console.log(err)
            })
    }

    /**
     * Get some statistics for authors
     */
    const getStatistics = () => {
        axios.get('statistics/authors').then(resp => {
            statistics.value = resp.data
        }).catch(err => {
            console.log(err)
        })
    }

    /**
     * Get author by router param id
     */
    const getAuthor = () => {
        axios.get('authors/' + route.params.id)
            .then(resp => {
                author.value = resp.data
            })
            .catch(err => {
                console.log(err)
            })
    }

    /**
     * Remove author by id
     * @param {integer} id 
     */
    const remove = (id) => {
        return new Promise((resolve) => {

        axios.delete('authors/' + id)
            .then(() => {
                authors.value = authors.value.filter(author => {
                    return author.id !== id;
                })
            })
            .catch(err => {
                console.log(err)
            }).finally(() => {
                resolve()
            })
        })
    }

    /**
     * Update author
     */
     const update = () => {
        axios.put(
            'authors/' + route.params.id,
            {
                author: author.value
            }
        ).then(() => {
            router.push({name: 'Authors.Grid'})
        }).catch(err =>{
            errors.value = err.response.data.errors
        })
    }

    /**
     * Create new author
     */
    const create = () => {
        axios.post(
            'authors',
            {
                author: author.value
            }
        ).then(resp => {
            author.value = resp.data
            router.push({name: 'Authors.Grid'})
        }).catch(err => {
            errors.value = err.response.data.errors
        })
    }

    return {
        author,
        authors,
        create,
        getAll,
        remove,
        getAuthor,
        update,
        errors,
        responseData,
        getStatistics,
        statistics
    }

}
export default Authors