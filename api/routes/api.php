<?php

use App\Http\Controllers\AuthorController;
use App\Http\Controllers\AuthorStatisticsController;
use App\Http\Controllers\BookController;
use App\Http\Controllers\BookStatisticsController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::apiResource('books', BookController::class);
Route::apiResource('authors', AuthorController::class);
Route::get('statistics/books', [BookStatisticsController::class, 'defaultBooksStatistics'])->name('books.statistics');
Route::get('statistics/authors', [AuthorStatisticsController::class, 'defaultAuthorsStatistics'])->name('authors.statistics');
