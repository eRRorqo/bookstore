import { createRouter, createWebHistory } from 'vue-router'

const routes = [
  {
    path: '',
    name: 'Index'
  },
  {
    path: '/books',
    name: 'Books',
    meta: {
      breadcrumb: 'Knihy'
    },
    component: () => import(/* webpackChunkName: "books" */ '../views/Books.vue'),
    children: [
      {
        path: '',
        name: 'Books.Grid',
        meta: {
          breadcrumb: 'Tabulka'
        },
        component: () => import(/* webpackChunkName: "books-grid" */ '../views/Books/Index.vue')
      },
      {
        path: 'create',
        name: 'Books.Create',
        meta: {
          breadcrumb: 'Pridať novú knihu'
        },
        component: () => import(/* webpackChunkName: "books-create" */ '../views/Books/Create.vue')
      },
      {
        path: ':id/edit',
        name: 'Books.Edit',
        meta: {
          breadcrumb: 'Upraviť knihu'
        },
        component: () => import(/* webpackChunkName: "books-edit" */ '../views/Books/Edit.vue')
      }
    ]
  },
  {
    path: '/authors',
    name: 'Authors',
    meta: {
      breadcrumb: 'Autori'
    },
    component: () => import(/* webpackChunkName: "authors" */ '../views/Authors.vue'),
    children: [
      {
        path: '',
        name: 'Authors.Grid',
        meta: {
          breadcrumb: 'Tabulka'
        },
        component: () => import(/* webpackChunkName: "authors-index" */ '../views/Authors/Index.vue')
      },
      {
        path: 'create',
        name: 'Authors.Create',
        meta: {
          breadcrumb: 'Pridať nového autora'
        },
        component: () => import(/* webpackChunkName: "authors-create" */ '../views/Authors/Create.vue')
      },
      {
        path: ':id/edit',
        name: 'Authors.Edit',
        meta: {
          breadcrumb: 'Upraviť autora '
        },
        component: () => import(/* webpackChunkName: "authors-edit" */ '../views/Authors/Edit.vue')
      }
    ]
  }
 
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

router.beforeEach((to) => {
  if(to.name == 'Index') {
    router.push({name: 'Books.Grid'})
  }
})

export default router
