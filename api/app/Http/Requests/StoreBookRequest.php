<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreBookRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'book.title' => 'required|unique:books,title',
            'book.author_id' => 'required|exists:authors,id',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'book.title.required' => trans('book.title.required'),
            'book.title.unique' => trans('book.title.unique'),
            'book.author_id.required' => trans('book.author_id.required'),
            'book.author_id.exists' => trans('book.author_id.exists'),
        ];
    }
}
