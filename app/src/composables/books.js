import { ref } from 'vue'
import axios from "axios"
import {useRouter, useRoute} from 'vue-router'

const Books = () => {

    const router = useRouter();
    const route = useRoute();

    let responseData = ref([]);
    let errors = ref([]);
    let statistics = ref({})

    let books = ref([]);
    let book = ref({
        id: null,
        title: "",
        author_id: 0
    })

    /**
     * Get all books with pagination
     * @param {string} urlLink 
     */
    const getAll = (urlLink = 'books') => {
        axios.get(urlLink)
            .then(response => {
                books.value = response.data.data 
                responseData.value = response.data
            })
            .catch(err => {
                console.log(err)
            })
    }

    /**
     * Get some statistics for boos
     */
    const getStatistics = () => {
        axios.get('statistics/books').then(resp => {
            statistics.value = resp.data
        }).catch(err => {
            console.log(err)
        })
    }

    /**
     * Get book by router param id
     */
     const getBook = () => {
        axios.get('books/' + route.params.id)
            .then(resp => {
                book.value = resp.data
            })
            .catch(err => {
                console.log(err)
            })
    }

    /**
     * Remove book by ID
     * @param {integer} id 
     */
    const remove = (id) => {
        return new Promise((resolve) => {

        axios.delete('books/' + id)
            .then(() => {
                books.value = books.value.filter(book => {
                    return book.id !== id;
                })
            })
            .catch(err => {
                console.log(err)
            }).finally(() => {
                resolve()
            })
        })
    }

    /**
     * Update Book
     */
     const update = (book) => {
         return new Promise((resolve) => {
            axios.put(
                'books/' + book.id,
                {
                    book: book
                }
                ).then(() => {
                    router.push({name: 'Books.Grid'})
                }).catch(err =>{
                    errors.value = err.response.data.errors
                }).finally(() => {
                    resolve()
                })
        })
    }


    /**
     * Create new Book
     */
     const create = () => {
        axios.post(
            'books',
            {
                book: book.value
            }
        ).then(resp => {
            book.value = resp.data
            router.push({name: 'Books.Grid'})
        }).catch(err =>{
            errors.value = err.response.data.errors
        })
    }

    return {
        books,
        getAll,
        remove,
        book,
        create,
        errors,
        getBook,
        update,
        responseData,
        getStatistics,
        statistics
    }

}
export default Books